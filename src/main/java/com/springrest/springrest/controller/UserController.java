package com.springrest.springrest.controller;

import com.springrest.springrest.controller.view.UserForm;
import com.springrest.springrest.controller.view.UserView;
import com.springrest.springrest.model.User;
import com.springrest.springrest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")

public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Collection<UserView> getAll() {
        return userService
                .getAll()
                .stream()
                .map(UserView::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping(path = {"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UserView findUserById(@PathVariable Long id) {
        User findUser = userService.findUserById(id);
        return UserView.fromEntity(findUser);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserView createUser(@RequestBody UserForm userForm) {
        User user = userForm.toEntity();
        User userSaved = userService.createUser(user);
        return UserView.fromEntity(userSaved);
    }
}
