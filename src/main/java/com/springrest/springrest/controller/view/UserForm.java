package com.springrest.springrest.controller.view;

import com.springrest.springrest.model.User;

import java.util.Date;

public class UserForm {
    private String nome;
    private String email;
    private String cpf;
    private Date dataNascimento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    // formulário -> entidade
    public User toEntity() {
            User user = new User();
            user.setNome(this.getNome());
            user.setCpf(this.getCpf());
            user.setEmail(this.getEmail());
            user.setDataNascimento(this.getDataNascimento());
            return user;
    }
}
