package com.springrest.springrest.controller.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.springrest.springrest.model.User;

import java.util.Date;

public class UserView {
    private String nome;
    private String email;
    @JsonIgnore
    private String cpf;
    private Date dataNascimento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    // entidade -> objeto de visualização
    public static UserView fromEntity(User user) {
        UserView userView = new UserView();
        userView.setNome(user.getNome());
        userView.setCpf(user.getCpf());
        userView.setEmail(user.getEmail());
        userView.setDataNascimento(user.getDataNascimento());
        return userView;
    }
}
