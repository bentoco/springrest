package com.springrest.springrest.repository;

import com.springrest.springrest.model.User;
import org.springframework.data.repository.CrudRepository;
import java.util.Collection;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByCpf(String cpf);
    Collection<User> findAll();
}

