package com.springrest.springrest.service;

import com.springrest.springrest.exceptions.NotFoundException;
import com.springrest.springrest.exceptions.UserAlreadyExistsException;
import com.springrest.springrest.model.User;
import com.springrest.springrest.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Collection<User> getAll(){
        return userRepository.findAll();
    }

    public User findUserById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("User %d not found", id)));
    }

    public User createUser(User user) {
        User userExists = userRepository.findByCpf(user.getCpf());
        if (userExists != null)
        {
            throw new UserAlreadyExistsException(String.format("User already exists."));
        }
        return userRepository.save(user);
    }
}
   
