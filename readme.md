# **Spring Boot e Spring Data JPA com MySQL - REST Api**

## SpringRest API

Neste post será criado uma aplicação com o auxílio do **Spring Boot Framework**, ferramenta que facilita o processo de configuração de projetos desenvolvidos em Java.
___
- [Overview](#overview)
- [Dependências](#dependências)
- [Entidade](#entidade)
- [Repositório](#repositório)
- [Configurando acesso ao MySQL](#configurando-acesso-ao-mysql)
- [Controller](#controller)
- [View](#userview)
- [Service](#service)
- [Exceptions](#exceptions)
- [Executando](#executando)
- [Testando](#testando)
- [Conclusão](#conclusão)
---

## Proposta

Criar **API RESTful** que atenda os seguintes requisitos:
- Criar um novo usuário - @PostMapping("/users")
- Listar todos os usuários - @GetMapping("/users")
- Obter um usuário específico pelo ID - @GetMapping("/users")
___

## Overview

Para um melhor entendimento, projetei um diagrama onde apresenta o fluxo de dados da aplicação, figura abaixo:

![Diagrama](https://i.imgur.com/Yyal7l4.png)

Sempre que houver alguma requisição **HTTP**, a camada [`Controller`](#controller) será responsável por recebê-las e gerenciá-las. Esta camada trafega nossos dados entre as demais camadas e controla as informações exibidas para o usuário.

Por boa prática, não devemos acoplar a camada de persistência diretamente no controlador, sendo assim, 
criamos uma camada `View`, na qual teremos métodos responsáveis por limitar os dados trafegados, transformando Entidade em [Objeto View](#userview) ou [Objeto Form](#userform) em Entidade.

Já a layer [`Service`](#service) centraliza as regras de negócio. Nela tratamos os dados devidamente e nos comunicamos com o repositório.

A camada de [`Repository`](#repositrio-jpa) tem um papel muito importante em nossa aplicação: ela é responsável por ser o meio de transporte entre a aplicação e o banco de dados. Com a ajuda do **Spring Data JPA** e **Hibernate**, conseguimos mapear entidades (classes Java) e tabelas do banco de dados de uma maneira orientada a objetos, o que facilita muito o desenvolvimento, pois não precisamos escrever queries `SQL` manualmente.

---
## Projeção

### Toolchain

Para o desenvolvimento desse projeto foram utilizadas as seguintes ferramentas:
- **Java™ Development Kit (JDK)** versão 11.
- **MySQL**, optei utilizá-lo com **Docker** que nos possibilita utilizar banco de dados containerizado.
- **Spring Initializr** [start.spring.io](https://start.spring.io/), ferramenta que nos auxilia a criar um novo projeto com as dependências necessárias do **Spring Boot Framework**. 
---

### Dependências

Optei em utilizar o Maven para lidar com as dependências do projeto, configuradas dentro do arquivo `pom.xml`:

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
			<version>2.4.1</version>
		</dependency>

		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>8.0.22</version>
		</dependency>
```
- **Spring Boot Starter Web:** Usado na construção de aplicações web, incluindo aplicações RESTful usando Spring MVC.  
- **Spring Boot Starter Data JPA:** Para gerenciar dados relacionais em aplicações Java. Ele nos permite acessar e persistir dados entre o objeto / classe Java e o banco de dados relacional.
- **MySQL Connector Java**: JDBC(Java Database Connectivity). API que facilita o acesso de nossa aplicação Java com a base de dados

---
### Entidade

Criando a classe de entidade JPA:

```java
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nome;
    private String email;
    private String cpf;
    private Date dataNascimento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
```
A anotação `@Entity` é utilizada para informar que uma classe também é uma entidade. A partir disso, a JPA estabelecerá a ligação entre a entidade e uma tabela de mesmo nome no banco de dados, onde os dados de objetos desse tipo poderão ser persistidos.

Também é necessário identificar qual atributo é a chave primária, para isto basta adicionar a anotação `@Id`. E a anotação `@GeneratedValue` para identificar como a coluna id será gerado, nesse caso auto-generated.

---

### Repositório

Utilizamos `@Repository` para anotar a interface `UserRepository` que extende a interface `CrudRepository` do Spring Data JPA na camada de persistência, que atua como repositório de dados e disponibiliza métodos para as operações CRUDs.

Dentro da interface, os seguintes métodos foram implementados:
- `findByCpf()` - obtem um usuário específico através do CPF.
- `findAll()` - retorna todos os usuários.

```java
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByCpf(String cpf);
    Collection<User> findAll();
}
```
___
### Configurando acesso ao MySQL

Para que o código consiga se conectar com sucesso na base de dados `MySQL`, é necessário informar os parâmetros da conexão. Esses parâmetros são declarados no arquivo *src/main/resources/**application.properties***:

```properties
spring.datasource.url = jdbc:mysql://mysql-docker-container:4022/spring_examples_db?useSSL=false
spring.datasource.username = app_user
spring.datasource.password = user1234
```
Devemos também definir o dialeto que será utilizado pelo **Hibernate**, a fim de otimizar os comandos `SQL` utilizados pelo framework:
```properties
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect
```
Caso deseje que o Hibernate crie as tabelas automaticamente com base no entidade da aplicação, devemos definir a estratégia de **DDL**:
```properties
spring.jpa.hibernate.ddl-auto=update
```
___

### Controller

`UserController` é a classe responsável por expor as **APIs** de `User` e trafegar os dados para a camada de `Service`.

```java
@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Collection<UserView> getAll() {
        return userService
                .getAll()
                .stream()
                .map(UserView::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping(path = {"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UserView findUserById(@PathVariable Long id) {
        User findUser = userService.findUserById(id);
        return UserView.fromEntity(findUser);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserView createUser(@RequestBody UserForm userForm) {
        User user = UserForm.toEntity(userForm);
        User userSaved = userService.createUser(user);
        return UserView.fromEntity(userSaved);
    }
}
```
A anotação `@RestController` é combinação das classes `@Controller` e `@ResponseBody`.
- `@Controller` - URIs que serão expostas pela API
- `@ResponseBody` - indexa o valor retornado pelos métodos no corpo de resposta (response body).
- `@RequestMapping` - anotação que nos permite informar as rotas da API e utilizar os métodos HTTP: `@GetMapping, @PostMapping, @PutMapping, @DeleteMapping, @PatchMapping`

No trecho abaixo é utilizada a técnica de inversão de controle (injeção de dependência) para injetar uma instância de `UserService` na classe `UserController`:
```java
private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
``` 
A class `UserController` possui três métodos, conforme especificado no início do artigo [aqui](#proposta). A camada Controller nunca deve retornar a entidade `User` diretamente para o **Client**. Isso nos permite controlar os dados que são retornados para frontend, desacoplar o frontend do banco de dados, entre outros benefícios.
 
Portanto, criamos uma camada `View` que possui uma classe `UserView`, que conversa com nossa entidade `User` através do método `fromEntity()`, com isso podemos tratar os dados retornados para o usuário.

### `UserView();`

```java
public class UserView {
    private String nome;
    private String email;
    @JsonIgnore
    private String cpf;
    private Date dataNascimento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public static UserView fromEntity(User user) {
        UserView userView = new UserView();
        userView.setNome(user.getNome());
        userView.setCpf(user.getCpf());
        userView.setEmail(user.getEmail());
        userView.setDataNascimento(user.getDataNascimento());
        return userView;
    }
}
```
Perceba que com a anotação `@JsonIgnore` eu consigo ocultar o CPF no retorno para usuário, algo que não poderiamos fazer com a entidade, pois se trata de uma propriedade do cadastro.

Criamos também uma classe `UserForm` que converte os dados vindos no body da requisição em entidade, através do método `toEntity()`:

### `UserForm();`
```java
public class UserForm {
    private String nome;
    private String email;
    private String cpf;
    private Date dataNascimento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public static User toEntity(UserForm userForm) {
            User user = new User();
            userForm.setNome(userForm.getNome());
            userForm.setCpf(userForm.getCpf());
            userForm.setEmail(userForm.getEmail());
            userForm.setDataNascimento(userForm.getDataNascimento());
            return user;
    }
}
```
___

### Service

Costumamos utilizar a camada de serviço, entre o controlador e o repositório, na qual ficam as regras de negócio e a chamada ao respository.
A camada de `Service` comunica-se com `Repository` via injeção de dependência, permitindo utilizar os métodos disponibilizados pela classe. Também temos os seguintes recursos:
- `getAll` - retorna todos usuários por meio do método `findAll`.
- `findUserById` - busca usuário específico pelo id. Caso o registro não seja encontrado é lançado uma [`NotFoundException`](#notfoundexception).
- `createUser` - método responsável por criar usuários. Primeiramente, verifica se o CPF do usuário que está sendo criado já existe, se houver, [`UserAlreadyExistsException`](#useralreadyexistsexception) será lançada. Caso contrário, os dados serão persistidos com sucesso.

```java
@Service
public class UserService {
    private UserRepository userRepository;

    public UserService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Collection<User> getAll(){
        return userRepository.findAll();
    }

    public User findUserById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("User %d not found", id)));
    }

    public User createUser(User user) {
        User userExists = userRepository.findByCpf(user.getCpf());
        if (userExists != null)
        {
            throw new UserAlreadyExistsException(String.format("User already exists."));
        }
        return userRepository.save(user);
    }
}
```
*Algo interessante de ressaltar é que `findByCpf()` não é um dos métodos que **JPA** disponibiliza nativamente, mas podemos criar métodos conforme necessidade e deixar a responsabilidade pela criação da querie com o **JPA**, [repositório](#repositório).*
___
### Exceptions

Camada de classes criadas para tratarmos as exceções lançadas na aplicação:


### `NotFoundException();`


```java
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super();
    }

    public NotFoundException(String message) {
        super(message);
    }
}
```
### `UserAlreadyExistsException();`


```java
@ResponseStatus(HttpStatus.CONFLICT)
public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException() {
        super();
    }
    public UserAlreadyExistsException(String message) {
        super(message);
    }

}
```
Para isto extendemos a super classe `RuntimeException` com ela podemos lançar exceções durante as operações realizadas na aplicação. Na anotação `@ResponseStatus` podemos informar qual será o respectivo **HTTP Status Code**.

___
### Executando

Vamos construir e executar o programa. Abra uma linha de comando (ou terminal) e navegue até a pasta onde você tem os arquivos do projeto. Podemos construir e executar o aplicativo com o seguinte comando: 

MacOS/Linux:
```bash
./mvnw spring-boot:run
```

Windows:
```bash
mvnw spring-boot:run
```
___

### Testando

`createUser();`


```bash
curl -X POST http://localhost:8080/users -H 'content-type:application/json' -d '{
    "nome": "Keanu Reeves",
    "email": "keanu@gmail.com",
    "cpf": "49911020810",
    "dataNascimento": "20161109103000"
}'
```

Response:
```json
{
    "nome": "Keanu Reeves",
    "email": "keanu@gmail.com",
    "dataNascimento": "20161109103000"
}
```

`findById();`

```bash
curl http://localhost:8080/users
```
Response:
```json
{
  "_embedded" : {
    "users" : [ {
      "nome": "Keanu Reeves",
      "email": "keanu@gmail.com",
      "cpf": "49911020810",
      "dataNascimento": "19851109103000"
    }
  }
}
```

`getAll();`

```bash
curl http://localhost:8080/users/1
```
Response:
```json
{
  "_embedded" : {
    "users" : [ {
      "nome": "Keanu Reeves",
      "email": "keanu@gmail.com",
      "cpf": "49911020810",
      "dataNascimento": "20161109103000"
    }, {
      "nome": "Will Smith",
      "email": "will@gmail.com",
      "cpf": "54921939310",
      "dataNascimento": "19881109103000"
    }
  }
}
```
___

### Conclusão

Em suma, quando precisamos utilizar a linguagem Java em algum projeto, `Spring Boot Framework` é uma boa alternativa, pois a injeção dos *beans* nos traz praticidade e ajuda a desenvolver aplicações de qualidade e mais rapidamente, além da facilidade de configuração com componentes externos através do `application.properties`. Outro facilitador muito grande são as annotations, que nos permitem criar rotas de API e métodos que escutam determinados verbos HTTP.
___

### Autor

Gabriel Bento
- Mail: bentoco@outlook.com.br
- Link do repositório: [Gitlab - Projeto Springrest](https://gitlab.com/bentoco/springrest)

___

[[↑Início↑]](#springrest-api)











 








